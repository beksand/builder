package pl.sda.designpatterns.builder.zad1;

public class Main {
    public static void main(String[] args) {
        GameCharacter gameCharacter = new GameCharacter.Builder().setName("Jurek").setMana(100).setNumberOfPoints(12).create();
    }
}
